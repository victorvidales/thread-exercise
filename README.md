## Porducer-Consumer problem overview
On this project we solved the Producer-Consumer problem, which is a common exercise to explain the use of threads.
The problem consist in two processes that work at the same time on a *queue*, one of them is the *producer* and the other one is the *consumer*, their roles are the next ones:
1. **Producer**: it has the role of send items to the queue.
2. **Consumer**: it has the role of take each item that is on the queue.
That is the main behavior of the program, but the problem is that we have to prevent the producer from putting more items on the queue that exceeds its maximum capacity, and prevent the consumer from getting items form the queue when it is empty.

---

## Solution
The problem´s solution that we applied consist in the use of four clases and semaphores. The classes and their explanation are below:
1. **Q**: This class synchronizes the producer and the consumer, ensuring that the producer adds an element to the queue and after that the consumer can obtain it.
2. **Producer**: This class correspond to the producer process and is the one that puts items on the queue by the use of a method of the *Q* class.
3. **Consumer**: This class correspond to the consumer process and is the one that gets items of the queue by the use of a method of the *Q* class.
4. **App**: This class is the main one, which has the role of creating an instance of each one of the previous classes, so they can to work together.
Now, let´s review de functionality of each class.

**Q class**
It has an integer attribute named *item* that represent de items that are going to enter and exit of the queue, it also has two *static* attributes of type *Semaphore*, one of them named *semCon* (used by the consumer process) and the other one named *semnProd* (used by the producer process), they are initialized as shown below:
	*static Semaphore semCon = new Semaphore(0);
    static Semaphore semProd = new Semaphore(1);*
The number that is passed in the constructor indicates the number of permits that is the number of threads that can shared the resource, so it assures that the *semProdu* attribute is going to be executed first because the *semCon* attribute does not have any thread allow to use it.
Then, the class shows two methods that are the next ones:
1. **void get()**: this method is the one that is going to be used by the consumer in order to getting items from the queue, it works by the use of the *semCon* attribute.
2. **void put(int item)**: this method is the one that is going to be used by the producer in order to putting items in the queue, it works by the use of the *semProd* attribute.
Both methods work similar, first each method have to acquire a permit in order to execute it´s process so, as mentioned earlier, the *semnProd* atttribute has been initialized with a permit so that method can begin, but the other method has been initialized without one, as a consequence it has to wait for the producer to release that permit, so it would be able to be used.
Once the attribute of the method has a permit it could make it´s process, the *get* method will get the item that the producer has left on the *item* attribute, and the *put* method will change the value of the *item* attribute to a new one.
Finally, both metodhs end calling the method *release()*, this method releases the permit of the semaphore, so the other one can aquire it to excecute it´s own process.
As you can see, this class has the role of controling the execution of the producer and consumer by the use of a * permit * that allows to execute one proccess at a time and ensuring that the producer starts by initializating the Semaphores with the correct values.

**Producer class**
This class correspond to the producer process, it implements the *Runnable* interface that allows the class to be executed as a thread. It has a constructor that recives an object of type *Q*, so it allows to link the producer to the *Q* object that will control the moment in which the producer is going to put a new item on the *item* attribute, so the consumer can take it.
It has a method named *run()* that is going to be run once the thread starts, this method contanis a loop that is going to call the *put* method of the *Q* object and it´s going to send a new value, so it can be updated in the *item* attribute of the *Q* class.
It´s important to say that this method is going to be executed when the *semProdu* attribute of the *Q* object aquires a *permit*, meanwhile it will wait.

**Consumer class**
This class correspond to the consumer process, and it has the same workflow as the *Producer* class, the difference is that, in the *run* method, it calls the *get* method of the *Q* object, so it can get the value of the *item* attribute of the *Q* object.
It also implements the *Runnable* interface that allows the class to be executed as a thread and it has a constructor that recives an object of type *Q*, so it allows to link the consumer to the *Q* object that will control the moment in which the consumer is going to ger the *item* attribute, so the producer can put another one.
It also has a method named *run()* that works in the same way, it is going to be run once the thread starts, this method contanis a loop that is going to call the *get* method of the *Q* object and it´s going to get the current value of the *item* attribute.
It´s important to say that this method is going to be executed when the *semCon* attribute of the *Q* object aquires a *permit*, meanwhile it will wait.

**App class**
This class has the role of makeing an instance of each one of the previous classes and send the same *Q* object to the producer and consumer so they can be sincronized by the *Q* object.